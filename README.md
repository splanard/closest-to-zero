# closest-to-zero

Small app developed as a Java/Gradle training exercise.

Given an array of positive and negative integers, this app returns the closest number to zero.

## Usage

### Run

To run the application, use the following command line :

`./gradlew run --args'<the list of integers here>'`

Example : `./gradlew run --args'8 5 10'` will return 5.

### Test

To launch the unit tests, use the following command line :

`./gradlew clean test`