import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ClosestToZeroTest {

    @ParameterizedTest
    @MethodSource("testSet")
    void closestToZero( int[] input, int expectedOutput ){
        assertEquals( expectedOutput, Main.closestToZero( input ) );
    }
    @ParameterizedTest
    @MethodSource("testSet")
    void closestToZero_noStream( int[] input, int expectedOutput ){
        assertEquals( expectedOutput, Main.closestToZero_noStream( input ) );
    }

    static Stream<Arguments> testSet(){
        return Stream.of(
                // just positive values
                Arguments.of( new int[]{ 8, 5, 10 }, 5 ),
                // positive and negative values
                Arguments.of( new int[]{ 5, 4, -9, 6, -10, -1, 8 }, -1 ),
                // two of the integers have the same absolute value but opposite sign -> the positive one is expected
                Arguments.of( new int[]{ 8, 2, 3, -2 }, 2 ),
                // ... no matter in which order these 2 opposite integers are given !
                Arguments.of( new int[]{ -8, -2, 3, 2 }, 2 ),
                // empty array -> result 0 is expected
                Arguments.of( new int[0], 0 ),
                // null argument -> result 0 is expected
                Arguments.of( null, 0 )
        );
    }


}
