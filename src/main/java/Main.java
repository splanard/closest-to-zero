import java.util.Arrays;

import static java.lang.Math.abs;

/**
 * Coding exercise.
 */
class Main {

    private static final String ERROR = "This app expects an array of integers as arguments";

    public static void main( String[] args ){
        try {
            int[] functionInput = Arrays.stream( args ).mapToInt( Integer::parseInt ).toArray();
            System.out.println( "result: " + closestToZero( functionInput ) );
        } catch( RuntimeException e ){
            throw new IllegalStateException( ERROR );
        }
    }

    /**
     * Given an array of positive and negative integers, returns the closest number to zero.
     * If the closest number in input could be either negative or positive, the function returns the positive one.
     * If the input array is undefined or empty, the function returns 0.
     *
     * @param input An array of positive and negative integers.
     * @return the closest value to zero.
     */
    public static int closestToZero( int[] input ){
        if( input == null || input.length == 0 ){
            return 0;
        }
        return Arrays.stream( input )
                .boxed()
                .sorted( (a, b) -> abs(a) == abs(b) ? b-a : abs(a)-abs(b) )
                .iterator().next();
    }

    /**
     * Same function, but without using <code>Stream</code>.
     * @param input n array of positive and negative integers.
     * @return the closest value to zero.
     */
    public static int closestToZero_noStream( int[] input ){
        if( input == null || input.length == 0 ){
            return 0;
        }
        int min = Integer.MAX_VALUE;
        for( int t : input ){
            if( abs(t - 0.1) < abs(min - 0.1) ){
                min = t;
            }
        }
        return min;
    }

}